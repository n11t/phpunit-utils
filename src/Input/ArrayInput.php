<?php
declare(strict_types=1);

namespace N11t\PHPUnit\Input;

abstract class ArrayInput
{

    /**
     * Set the properties with an array.
     *
     * ArrayInput constructor.
     * @param array $properties The properties to set.
     */
    public function __construct(array $properties = [])
    {
        $properties = array_merge($this->getDefaultProperties(), $properties);

        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    /**
     * Get the default properties to set.
     *
     * @return array
     */
    abstract protected function getDefaultProperties(): array;
}
