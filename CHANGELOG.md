# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2017-03-19
### Added
- Added flag to delete folder in tearDown. Override `isCleanUpEnabled` to enable deletion.

## [0.3.0] - 2018-03-15
### Added
- New function to get content from asset file `assetGetContent`

### Changed
- Improved exceptionhandling
